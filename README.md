# Social Engineering Defense

## Purpose

The purpose of this project is to detect scam e-mails based on text analysis. This repository includes e-mail crawling, sentence processing, sentence type identification, form item detection, command analysis and question analysis. We used open-source projects for each steps as follows.

- Crawling
  - Beautifulsoup for crawling e-mails
  - Langid for selecting English e-mails
- Sentence Processing
  - Punctuator2 for adding punctuations
  - Punkt Algorithm(nltk) for sentence tokenizing
- Sentence Type Identification
  - Corenlp for drawing word dependency tree
  - Stanford Parser for POS tagging
- Question Analysis
  - Paralex for detecting question scams.
- Command Analysis
  - edu.mit.jwi_2.4.0.jar for using wordnet
  - gson-2.8.0.jar for using json
  - commons-lang-2.6.jar for capitalization
  - Corenlp for drawing word dependency tree
  - Stanford Parser for POS tagging

The structure of our social engineering defense system is as shown in the following figure.

## System Structure

![system_structure](/images/system_structure.png)

### Data

We use e-mail data, but input can be any text data. You can crawl e-mail data in the [crawling folder](https://gitlab.com/security-defenders/social-engineering-defense/tree/master/email_dataset/crawling), or you can use [pre-crawled e-mail data](https://drive.google.com/file/d/1D8BUS_wxZVip6EFmhMkrXunBXcuBev7o/view?usp=sharing).

### Sentence Processing

Analyzing scam data is difficult because they destroy grammar on purpose. The most critical point is they don't have right punctuations. It is really hard to break down into sentences without punctuations. We use [punctuator2](https://github.com/ottokart/punctuator2) for solving this problem. Then we detect sentence boundary with Punkt tool. It finds not only periods, but also can distinguish whether it is used for ending a sentence or abbreviation. You can see details in the [refining folder](https://gitlab.com/security-defenders/social-engineering-defense/tree/master/email_dataset/refining). You can use [pre-sentence-tokenized e-mail data](https://drive.google.com/open?id=1_LHcOVE0A3hd1mBwZl-o4ivSfu42IGJV).

### Form Item Detection

If your data has no form, you can skip this section. However, if you use our data or something with form, you should transform forms into questions.  
```
please fill this form
NAME : ___________________
JOB : ___________________
PHONE : _________________
```
This form can change to below questions.
```
what is your name?
what is your job?
what is your phone?
```

You can see details in the [form_item_detection folder](https://gitlab.com/security-defenders/social-engineering-defense/tree/master/form_item_detection).

### Sentence Type Identification

Scammers get information through questions and commands. Since we provide the different approach for these two categories, we used POS tagger which is provided from Stanford(coreNLP) to identify them. This tagger draws the syntactic and type dependency parse trees of sentences. We extract the commands in four cases below.

#### 1. Imperative
We find the imperative sentences which generally start with the verb.
```
Send me money!
Let me know your information.
```
#### 2. Suggestion
If there is a ‘you’ in front of the modal verb, we detect it as a command.
```
You should send me your address.
You must call him.
```
#### 3. DesireExpression
If the verb is included in desire verb, we detect it as a command.
```
I hope you will give me the money.
I want you to do these things.
```
#### 4. Question
If there are ‘SQ’ tag or ‘SBARQ’ tag in parse result, we detect it as a question.  
You can see the details in the [scam-question-detector folder](https://gitlab.com/learnitdeep/scam-question-detector).

### Check Malicious with Blacklist

We use blacklist which is made of TF-IDF for checking whether it's a scam or not.
```
transport money
ship money
send money
notify we
```
You can see the details in the [command_analyze folder](https://gitlab.com/security-defenders/social-engineering-defense/tree/master/command_analyze)

### Check Malicious with Question Answering System(Paralex)

We use [paralex](http://knowitall.cs.washington.edu/paralex/) system for this step. It is basically Question Answering System, but we changed the database to use it as question scam detector. Please download our [modified paralex file](https://drive.google.com/file/d/1XYXagUwkcKcFUU6Kljvh6zJAVSnHnM0t/view?usp=drive_web).  

```
unzip paralex-evaluation-test.zip
cd paralex-evaluation-test
./scripts/start_nlp.sh & # start nlp server
./scripts/start_demo.sh & # start demo server
```

Once the demo is running, you can make HTTP requests to Paralex and get JSON objects as output.

```
curl http://localhost:8083/parse?sent=What+is+your+password # "answers": ["confidential.e"]
curl http://localhost:8083/parse?sent=Who+invented+pizza # have no ["confidential.e"] answers
```


## Demo

Environment

- JAVA : JDK 8 Version
- Python : 2.7
- Python package : simplejson, bottle(pip install simplejson bottle)
- OS : Ubuntu
- Memory : 5GB

To run our demo, please download our [demo file](https://drive.google.com/file/d/1FxSNGqD8JDaGclDtQHLtXP_V_GPlFGCp/view?usp=sharing), and unzip it.  
If your operating system is <b>Windows</b>, you have to use <b>linux virtual machine</b>
```
unzip social-engineering-defense-demo.zip
```

For the demo, first, you need to run paralex server.  
Go to /paralex-evaluation-test and run shell scripts.  

```
cd social-engineering-defense-demo/paralex-evaluation-test
sh ./scripts/start_nlp.sh & # start nlp server
sh ./scripts/start_demo.sh & # start demo server
```

Our demo file is located in command_analyze/.  
First, Go to social-engineering-defense-demo/command_analyze.  
Then, run the Java program for demo. (In demo zip file, java files are already compiled.)
```
cd social-engineering-defense-demo/command_analyze
java -cp ./jar/*: temp
```
Now you can use our demo!
```
========================================================================================================  
START  
Enter a sentence(enter exit to terminate program)>> What is your password
...
>>Scam detected!!
Enter a sentence(enter exit to terminate program)>> What is your hobby
...
>>>Normal sentence
```

## Ongoing work
- Make punctuator faster(using different model)
- Collect other text data apart from emails
- improve form detecting algorithm
