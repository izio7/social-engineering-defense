import java.io.*;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

public class BlackList {
	
	private ArrayList<String[]> blacklist;
    String parserModel;
    LexicalizedParser lp;
	
	public BlackList() {
		// this.blacklist = readBlackList();
		this.parserModel = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
		this.lp = LexicalizedParser.loadModel(parserModel);
	}
	
	public ArrayList<String[]> getBlacklist() {
		return blacklist;
	}
	
	public void createBlackListFromJSON() {

		JSONParser parser = new JSONParser();
		ArrayList<String[]> blacklist = new ArrayList<String[]>();
		ArrayList<ArrayList<String>> blacklist1 = new ArrayList<ArrayList<String>>();
		
        try {
//        	temp.demoAPI(lp, sentence);

            // 해당 JSON 파일을 읽어옵니다.
            Object email = parser.parse(new FileReader(System.getProperty("user.dir")+ File.separator+"scam_imperative_0_29999.json"));
            // 이차원 JSONArray 이기 때문에 1차 JSONArray에 있는 2차 JSONArray Size를 측정해 줍니다.
            JSONArray email_array = (JSONArray) email;
            int size = email_array.size();
            
//            System.out.println(email_array);
            
            // mail count
            for(int i = 0; i < size; i++) {
                if(i % 1000 == 0){
                    System.out.println(i);
                }
            	
            	JSONArray jsonTemp = (JSONArray) email_array.get(i);
            	
            	// mail content
            	for(int j = 0; j < jsonTemp.size(); j++) {
            		
        			ArrayList<String> black = new ArrayList<String>();
        			String[] blackTemp = new String[5];
            		String sentence = (String) jsonTemp.get(j);
                    sentence = sentence.replace("please ","");
                    sentence = sentence.replace(" please","");
            		
        			blackTemp = temp.checkRecongMailList(lp, sentence);
        			
//        			System.out.println(blackTemp[0]);
	    			black.add(blackTemp[0]);
	    			black.add(blackTemp[1]);
	    			black.add(blackTemp[2]);
	    			black.add(blackTemp[3]);
	    			black.add(blackTemp[4]);
	    			blacklist1.add(black);
            	}
            }
            
            System.out.println(blacklist1);
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
//        System.out.println(blacklist1);
        //write data to json file
        
        String list = new Gson().toJson(blacklist1);
        
//        System.out.println(list);
        
        try {
            Writer file = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(System.getProperty("user.dir")+ File.separator+"scam_pair1.json"), "UTF8"));
           //FileWriter file = new FileWriter(System.getProperty("user.dir")+ File.separator+"scam_pair1.json");
            file.write(list);
            file.flush();
            file.close();
        } catch (IOException e) {
           e.printStackTrace();
        }
//        
		return;
	}
	
	private ArrayList<String[]> readBlackList() {
		
		JSONParser parser = new JSONParser();
		ArrayList<String[]> blacklist = new ArrayList<String[]>();
		
        try {

            // 해당 JSON 파일을 읽어옵니다.
            Object email = parser.parse(new FileReader(System.getProperty("user.dir")+ File.separator+"blacklist.json"));

            // 이차원 JSONArray 이기 때문에 1차 JSONArray에 있는 2차 JSONArray Size를 측정해 줍니다.
            JSONArray email_array = (JSONArray) email;
            int size = email_array.size();
            
            for(int i = 0; i < size; i++) {
            	
            	JSONArray jsonTemp = (JSONArray) email_array.get(i);
            	String[] blackTemp = new String[6];
            	
            	for(int j = 0; j < jsonTemp.size(); j++) {
            		blackTemp[j] = (String) jsonTemp.get(j);
            	}
            	
            	blacklist.add(blackTemp);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
		return blacklist;
	}
}
