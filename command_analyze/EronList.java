import java.io.*;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

public class EronList {
	
    String parserModel;
    LexicalizedParser lp;
    
    public EronList() {
		this.parserModel = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
		this.lp = LexicalizedParser.loadModel(parserModel);
    }
    
    public void createEronListFromJSON() {
    	
		JSONParser parser = new JSONParser();
		ArrayList<ArrayList<ArrayList<String>>> eronlist = new ArrayList<ArrayList<ArrayList<String>>>();
		
		try {
            // 해당 JSON 파일을 읽어옵니다.
            Object email = parser.parse(new FileReader(System.getProperty("user.dir")+ File.separator+"enron_imperative_0_29999.json"));
            // 이차원 JSONArray 이기 때문에 1차 JSONArray에 있는 2차 JSONArray Size를 측정해 줍니다.
            JSONArray email_array = (JSONArray) email;
            int size = email_array.size();
            
            // mail count
            for(int i = 0; i < size; i++) {
                if(i % 1000 == 0){
                    System.out.println(i);
                }
            	
            	JSONArray jsonTemp = (JSONArray) email_array.get(i);
            	ArrayList<ArrayList<String>> mail_eron_list = new ArrayList<ArrayList<String>>();
            	
            	// mail content
            	for(int j = 0; j < jsonTemp.size(); j++) {
            		
        			ArrayList<String> eron = new ArrayList<String>();
        			String[] eronTemp = new String[5];
            		String sentence = (String) jsonTemp.get(j);
                    sentence = sentence.replace("please ","");
                    sentence = sentence.replace(" please","");
        			eronTemp = temp.checkRecongMailList(lp, sentence);

	    			eron.add(eronTemp[0]);
	    			eron.add(eronTemp[1]);
	    			eron.add(eronTemp[2]);
	    			eron.add(eronTemp[3]);
	    			eron.add(eronTemp[4]);
	    			mail_eron_list.add(eron);
            	}
            	eronlist.add(mail_eron_list);
            }
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		
        String list = new Gson().toJson(eronlist);
        
//      System.out.println(list);
      
	      try {
              Writer file = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(System.getProperty("user.dir")+ File.separator+"enron_pair1.json"), "UTF8"));
	          //FileWriter file = new FileWriter(System.getProperty("user.dir")+ File.separator+"enron_pair1.json");
	          file.write(list);
	          file.flush();
	          file.close();
	      } catch (IOException e) {
	         e.printStackTrace();
	      }
		
    	return;
    }
    
}
