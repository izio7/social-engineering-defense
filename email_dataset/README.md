# Crawling

We provide pre-crawled emails. You can use our pre-crawled-email data in [google drive](https://drive.google.com/file/d/1D8BUS_wxZVip6EFmhMkrXunBXcuBev7o/view?usp=sharing).  
If you want to crawl emails by yourself, you can use our code.

## Scam Crawling

We crawl scam datas from three websites.  

1. scamalot_com.py => http://www.scamalot.com/ScamTipReports/
2. antifraudintl_org.py => http://antifraudintl.org/
3. scamwarners_com.py => https://www.scamwarners.com/forum/
4. scamdex.py => http://www.scamdex.com/

### Scamdex

We crawl scam email [here](http://www.scamdex.com) from 2007.10.16 to 2017.10.11. Some of them were not English and empty, so I parsed them with ${langid}^{[1]}$ and only for more then 10 characters.

- Total number of scam emails : 56555
- Total number of images : 8423

Most of images are not for the scam, but for the their fake logo.
crawling source code is [here](https://github.com/zerobugplz/social-engineering-defense/blob/master/crawling_scam_mails/scamdex.py).

1. [langid](https://github.com/saffsd/langid.py) is an accurate language distinguish library based on text data. more details are on their [paper](http://www.aclweb.org/anthology/P12-3005)

### Scamwarners

We crawl scam emails [here](http://www.scamwarners.com) from beginning to 2017.10.11. Since it is a community sites, there were some questions about scams, and giving information about scams. To avoid them, we crawled only texts which have simple email form ("From:").

- Total number of scam emails : 43241
- Total number of images : 471

crawling source code is [here](https://github.com/zerobugplz/social-engineering-defense/blob/master/crawling_scam_mails/scamwarners_com.py).

### Scamalot

We crawled scam emails [here](https://scamalot.com) from 2011.07.30 to 2017.10.11. It has also questions, information, same reason as Scamwarners. So that we crawled only texts which have scammer's email address.

- Total number of scam emails : 18149
- Total number of images : 69

crawling source code is [here](https://github.com/zerobugplz/social-engineering-defense/blob/master/crawling_scam_mails/scamalot_com.py).

### Antifraudintl

We crawled scam email [here](http://antifraudintl.org) from 2007.02.01 to 2017.10.12. We apply same algorithm as scamwarners.

- Total number of scam emails : 69209
- Total number of images : 754

crawling source code is [here](https://github.com/zerobugplz/social-engineering-defense/blob/master/crawling_scam_mails/antifraudintl_org.py).

### Total

Total number of scam email data is 187154.

### Python version

2.7.10

## Non Scam Crawling

### Enron Email

We use Enron email as non-scam-data which contains data from about 150 users, mostly senior management of Enron. You can get more details about Enron email data [here](https://www.cs.cmu.edu/~enron/). I randomly chose 187048 enron data because we have 187048 scam emails(some were empty out of 187154). You can download through this [link](https://drive.google.com/file/d/1huRLrUc7G1GdEfUb2t2rwAFoI9xlc3Wm/view?usp=sharing).

# Sentence Boundary Detection

We provide pre-sentence-tokenized emails. You can use our pre-sentence-tokenized email data in [google drive](https://drive.google.com/file/d/1tveWU5yungDuWlnBhlkfhkNM8CW21Xxw/view?usp=sharing).  
If you want to tokenize sentences by yourself, you can use our code.

## remove_header

We remove the email header to transform email data to text data.

## Puncatuate

### Motivaiton

Some of scam emails miss their punctuations, so it is hard to detect sentence boundary. We use punctuator for adding punctuations. You can see this algorithm on this [paper](http://www.isca-speech.org/archive/Interspeech_2016/pdfs/1517.PDF) and code [here](https://github.com/ottokart/punctuator2).  

### Idea

Punctuator2 can train model twice.  

1. Bidirectional GRU model with attention-mechanism, and late-fusion.
2. With late fusion output, add pause-duration and adapt to target domain, the second stage discards the first stage output layer and replaces it with a new recurrent GRU layer.

I used pre-trained model that they provide : [click](https://drive.google.com/drive/folders/0B7BsN5f2F1fZQnFsbzJ3TWxxMms)

### Code Description

Our code is on [github](https://github.com/zerobugplz/social-engineering-defense/tree/sentence_boundary_detection/sentence_boundary_detection/punctuator2-1.0). You should download [pre-training dataset](https://drive.google.com/drive/folders/0B7BsN5f2F1fZQnFsbzJ3TWxxMms) to run punctuator. Then just run [play_with_model.py](https://github.com/zerobugplz/social-engineering-defense/blob/sentence_boundary_detection/sentence_boundary_detection/punctuator2-1.0/play_with_model.py) with your text dataset. Because I use scam emails, and it has already punctuations partially, I change this punctuator2 code to punctuate only if there is no period or question mark nearby.  

### Training Details

- Adagrad optimizer : learning rate 0.02
- L2-norm < 2
- 5 epochs early termination
- Weight : normalizer initialization
- Hidden layer 256
- Activation function : tanh
- Mini-batches : 128
- Trained by Theano framework
- Data : [INTERSPEECH-T-BRNN-pre.pcl](https://drive.google.com/drive/folders/0B7BsN5f2F1fZQnFsbzJ3TWxxMms)

## Sentence Tokenizing with Punkt(NLTK).

### Motivation

It is one of the most popular sentence tokenizer algorithm in the world. It performs well and easy to use.

### Idea

![punkt_structure](https://gitlab.com/security-defenders/social-engineering-defense/tree/master/images/punkt_structure.png)  

- S : Sentence Boundary
- A : Abbreviation
- E : Ellipsis
- AS : Abbreviation at the End of Sentence
- ES : Ellipsis at the End of Sentence

### Code Description

Punkt algorithm has dependency with space between sentence and sentece because if sentences are not separated by period+spaces, but only with period, the algorithm detects it as an abbreviation in many cases. So I make a space when period is not used for abbreviation('Mr.', 'Ms.', 'Mrs.', 'www.', '@', 'Dr.', 'mr.', 'mrs.', 'dr.', 'Www.', 'http', 'Co.', and 'co.'). Just type below commands on your terminal.

```
pip install nltk
python punkt.py
```

# Training set and test set

We should devide data into two categories : training set and test set. We use training dataset for building our program, and test set is for evaluating.
